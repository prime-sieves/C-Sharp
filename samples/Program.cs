/*
   Copyright 2022 Program.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System.Diagnostics;
using System.Text;

namespace PrimeSieve;

using System;
    
public static class Program
{
    public static void Main()
    {
        var sieve = new PrimeSieve();
        Console.WriteLine("Prime sieve has been initialized and demonstration is ready.");
        RunSieve(sieve, 50, out var a);
        
        Console.WriteLine();
        Console.Write("Would you like to run the sieve again, but for 500? [Y/N] ");
        if (Console.ReadKey().Key == ConsoleKey.Y)
        {
            RunSieve(sieve, 500, out a);
        }
        
        Console.WriteLine();
        Console.Write("Would you like to run the sieve again, but for 5000? [Y/N] ");
        if (Console.ReadKey().Key == ConsoleKey.Y)
        {
            RunSieve(sieve, 5000, out a);
        }
        
        Console.WriteLine();
        Console.Write("Would you like to run the sieve again, but for 10000? [Y/N] ");
        if (Console.ReadKey().Key == ConsoleKey.Y)
        {
            RunSieve(sieve, 10000, out a);
            Console.WriteLine("This is the highest that this example has been programmed to go. Any higher and your device my be damaged by the sheer awesomeness that is large primes.");
        }
        
        Console.WriteLine("Prime sieve example application has reached the end of its operation. Good bye.");
    }

    private static void RunSieve(PrimeSieve sieve, int max, out long mills)
    {
        var stopwatch = new Stopwatch();
        Console.WriteLine();
        Console.Write($"Press any key to begin sieve to find all primes less than {max}... ");
        Console.ReadKey();
        Console.WriteLine();
        Console.WriteLine("Beginning operation.");
        sieve.MaxValue = max;
        
        stopwatch.Start();
        var results = sieve.RunInSingleThread();
        stopwatch.Stop();
        mills = stopwatch.ElapsedMilliseconds;
        
        Console.Write($"Operation has been completed in {mills}ms. Found {results.Count} primes. Would you like them listed? [Y/N] ");
        if (Console.ReadKey().Key == ConsoleKey.Y)
        {
            Console.WriteLine();
            var builder = new StringBuilder();
            for (var i = 0; i < results.Count; i++)
            {
                var separator = i > 0 && i % 20 == 0 ? Environment.NewLine : ", ";
                builder.Append($"{results[i]}{separator}");
            }

            builder.Remove(builder.Length - 2, 2);
            Console.Write(builder.ToString());
        }
        
        Console.WriteLine();
    }
}
