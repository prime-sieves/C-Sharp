/*
   Copyright 2022 TaskDataTests.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System.Reflection.Metadata.Ecma335;

namespace PrimeSieveTests;

using System.Collections.Generic;

using PrimeSieve;

[TestFixture]
public class TaskDataTests
{
    public static IEnumerable<object[]> TestCases
    {
        get
        {
            // candidate value less than 2
            yield return new object[] { -1, 1 };
            yield return new object[] { 0, 1 };
            yield return new object[] { 1, 1 };
        }
    }

    public static TaskData KnownGoodValue => new (3);

    [Test]
    [TestCaseSource(nameof(TestCases))]
    public void ShouldThrowWithBadArgs (int candidate, int maxValue)
    {
        var expectedExceptionType = typeof(ArgumentException);

        TaskData foo;

        Assert.Throws(expectedExceptionType, () => { foo = new TaskData(candidate, 0); }, "Wrong exception type thrown.");
    }
}
