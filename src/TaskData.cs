/*
   Copyright 2022 TaskData.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace PrimeSieve;

public struct TaskData
{
    public int Candidate;

    public int CurrentValue;

    public bool HasFinished = false;

    public bool Result = false;

    public TaskData(int candidate, int currentValue) : this()
    {
        if (candidate < 2)
            throw new ArgumentException($"{nameof(candidate)}: {candidate} must be greater than 2.", nameof(candidate));
        
        this.Candidate = candidate;
        this.CurrentValue = currentValue;
    }
    
    public TaskData (int candidate) : this (candidate, 0) { }
}
