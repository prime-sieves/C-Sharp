# Prime Sieve, C# edition
This is a simple sieve using a few optimizations to demonstrate proficiency with the C# language. 

## Description
A few of the optimizations include a candidate number generator that only produces numbers that end in 1, 3, 7, or 9. It also only checks the prime numbers that have already been found.

## Usage
Within the samples folder is a Program.cs file. This is in the same namespace as the files in the src folder. To run the program, create a new project and import all the files from both folders. This does require .NET6 as well as C# 10 support. 

## Support
Any issues should be reported on the issues page here.

## Roadmap
As far as I am concerned, this project is complete. I may add XML documentation in the near future.  

## License
This is licensed under Apache 2.0
