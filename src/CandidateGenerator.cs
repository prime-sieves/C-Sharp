/*
   Copyright 2022 CandidateGenerator.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System.Collections;

namespace PrimeSieve;

using System.Collections.Generic;

public class CandidateGenerator : IEnumerable<int>
{
    private int _current = 0;
    
    private int _safetyNet = int.MinValue;
    public IEnumerator<int> GetEnumerator()
    {
        // This generator method cuts out 60% of the work seeing as every number that ends with a 2, 4, 5, 6, 8 or 0
        //      is divisible by 2 or 5.
        
        // pattern should be 1, 3, 7, 9, 1
        //            or, add 2, 4, 2, 2
        // where 'current' == 0, 1, 2, 3 (reset on 4)
        while (this._safetyNet < int.MaxValue)
        {
            this._safetyNet++;
            yield return this._current == 1 ? 4 : 2;
            this._current++;
            if (this._current == 4)
                this._current = 0;
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return this.GetEnumerator();
    }
}
