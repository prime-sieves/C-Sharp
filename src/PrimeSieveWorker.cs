/*
   Copyright 2022 PrimeSieveWorker.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System.Reflection.Metadata.Ecma335;

namespace PrimeSieve;

using System;
using System.Threading;
using System.Threading.Tasks;

public class PrimeSieveWorker
{
    private bool _isPrepped;
    
    private TaskData _taskData;

    internal readonly List<int> KnownPrimes = new List<int>() { 2 };

    public void InsertNewPrime(int prime)
    {
        var max = this.KnownPrimes[^1];
        if (prime < max)
            throw new ArgumentException(nameof(prime), $"{nameof(prime)} must be greater {max}");

        this.KnownPrimes.Add(prime);
    }

    public void PrepForRun(TaskData taskData)
    {
        this._taskData.Candidate = taskData.Candidate;
        this._taskData.CurrentValue = taskData.CurrentValue;
        this._taskData.HasFinished = false;
        this._isPrepped = true;
    }

    public TaskData Run(CancellationToken token)
    {
        if (!this._isPrepped)
            throw new InvalidOperationException($"Call {nameof(PrepForRun)} before call this method.");

        var result = default(TaskData);
        bool isPrime = true, isCancelled = false;
        int i, count = this.KnownPrimes.Count;
        for (i = 0; i < count; i++)
        {
            if (token.IsCancellationRequested)
            {
                isCancelled = true;
                break;
            }

            if (this._taskData.Candidate % this.KnownPrimes[i] == 0)
            {
                isPrime = false;
                break;
            }
        }

        result.Candidate = this._taskData.Candidate;
        result.CurrentValue = isPrime ? this.KnownPrimes[^1] : this.KnownPrimes[i];
        result.Result = isPrime;
        result.HasFinished = !isCancelled;

        return result;
    }
}
