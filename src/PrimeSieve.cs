/*
   Copyright 2022 PrimeSieve.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Reflection.Metadata.Ecma335;

namespace PrimeSieve;

using System;
using System.Collections.Generic;
using System.Threading;

public class PrimeSieve : IDisposable
{
    private const string MaxValueTooLowErrorMessagePredicate = "must be between 0 and 10 million.";
    
    private int _batchSize;

    [AllowNull]
    private IEnumerator<int> _candidateGenerator;

    private readonly ReaderWriterLockSlim _locker;
    
    private int _maxValue;

    private readonly Queue<int> _nextValues;

    public int MaxValue
    {
        get => _maxValue;
        set
        {
            if (value < 1)
                throw new ArgumentOutOfRangeException(nameof(value), $"This must be greater than 0.");
            
            DebugThrowIfOver10K(value);

            this._maxValue = value;

            [Conditional("DEBUG")]
            static void DebugThrowIfOver10K(int value)
            {
                if (value > 10000)
                    throw new ArgumentOutOfRangeException(nameof(value), $"This must be less than 10,000.");
            }
        }
    }

    public PrimeSieve()
    {
        this._locker = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        
        this._batchSize = Environment.ProcessorCount;
        this._nextValues = new Queue<int>(this._batchSize);
    }

    private static void SingleThreadRunWorkerHelper(int candidate, PrimeSieveWorker worker, CancellationToken token)
    {
        var taskData = new TaskData(candidate);
        worker.PrepForRun(taskData);
        taskData = worker.Run(token);
        if (taskData.Result)
            worker.InsertNewPrime(taskData.Candidate);
    }

    private void CreateCandidateValues(int startValue, int maxKnownPrime, int max)
    {
        var temp = startValue;
        while (this._nextValues.Count < this._batchSize & temp < maxKnownPrime * maxKnownPrime & temp <= max)
        {
            this._candidateGenerator.MoveNext();
            temp += this._candidateGenerator.Current;
            this._nextValues.Enqueue(temp);
        }
    }

    private void CreateInitialCandidateValues()
    {
        this._nextValues.Enqueue(3);
        this._nextValues.Enqueue(5);
        this._nextValues.Enqueue(7);
    }

    private IEnumerable<Task<TaskData>> QueueWorkerTasks(PrimeSieveWorker[] workers, CancellationTokenSource cancellationTokenSource)
    {
        var i = 0;
        while (this._nextValues.Count > 0)
        {
            var candidate = this._nextValues.Dequeue();
            var taskData = new TaskData(candidate);
            var worker = workers[i];
            worker.PrepForRun(taskData);

            yield return Task.Run(() => worker.Run(cancellationTokenSource.Token));

            i++;
        }
    }

    private void ThrowIfNotReady()
    {
        if (this._maxValue < 1)
            throw new InvalidOperationException($"{nameof(MaxValue)} {MaxValueTooLowErrorMessagePredicate}");
    }

    public void Dispose()
    {
        _candidateGenerator.Dispose();
        _locker.Dispose();
    }

    public IReadOnlyList<int> RunInSingleThread()
    {
        this.ThrowIfNotReady();
        var worker = new PrimeSieveWorker();
        var token = new CancellationToken(false);
        var candidate = 1;
        var max = this.MaxValue;
        this._candidateGenerator = new CandidateGenerator().GetEnumerator();
        // Note the first few numbers are a bit crazy. This could be rewritten to just make the call for 3 to 9 manually, but this uses the generator.
        this._candidateGenerator.MoveNext();
        candidate += this._candidateGenerator.Current; // 3
        SingleThreadRunWorkerHelper(candidate, worker, token);
        candidate += this._candidateGenerator.Current; // Hack to make this give a 5. The generator skips numbers ending in 5.
        SingleThreadRunWorkerHelper(candidate, worker, token);
        candidate += this._candidateGenerator.Current; // Hack to make this give a 7.
        this._candidateGenerator.MoveNext(); // Hack to make the rest of the numbers generate correctly.
        SingleThreadRunWorkerHelper(candidate, worker, token);
        while (candidate < max)
        {
            if (candidate == 43)
                candidate = 43;
            
            this._candidateGenerator.MoveNext();
            candidate += this._candidateGenerator.Current;
            SingleThreadRunWorkerHelper(candidate, worker, token);
        }

        return worker.KnownPrimes;
    }

    public IReadOnlyList<int> RunInMultiThread()
    {
        return this.RunInMultiThread(this._batchSize);
    }

    public IReadOnlyList<int> RunInMultiThread(int suggestedThreadCountToUse)
    {
        this.ThrowIfNotReady();
        var candidate = 1;
        var max = this.MaxValue;
        this.CreateInitialCandidateValues();
        this._candidateGenerator = new CandidateGenerator().GetEnumerator();
        this._candidateGenerator.MoveNext();
        this._candidateGenerator.MoveNext();
        var cancellationTokenSource = new CancellationTokenSource(); 
        this._batchSize = Math.Max(this._batchSize, suggestedThreadCountToUse);
        var results = new Task<TaskData> [this._batchSize];
        var workers = new PrimeSieveWorker [this._batchSize];
        for (var w = 0; w < this._batchSize; w++)
            workers[w] = new PrimeSieveWorker();

        while (candidate < max)
        {
            if (this._nextValues.Count == 0)
                this.CreateCandidateValues(candidate, workers[0].KnownPrimes[^1], max);

            var completedTasks = Task.WhenAll(QueueWorkerTasks(workers, cancellationTokenSource)).Result;
            foreach (var task in completedTasks)
            {
                candidate = task.Candidate;
                if (task.Result)
                    foreach (var worker in workers)
                        worker.InsertNewPrime(task.Candidate);
            }
        }

        return workers[0].KnownPrimes;
    }
}
