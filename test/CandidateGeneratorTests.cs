/*
   Copyright 2022 CandidateGeneratorTests.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace PrimeSieveTests;

using PrimeSieve;

[TestFixture]
public class CandidateGeneratorTests
{
    [Test]
    public void Test()
    {
        var testee = new CandidateGenerator();

        var i = 0;

        foreach (var actual in testee)
        {
            i++;
            if (i == 2 || i == 6 || i == 10)
                Assert.That(actual, Is.EqualTo(4));
            else
                Assert.That(actual, Is.EqualTo(2));
            
            if (i == 12)
                break;
        }
    }
}
