/*
   Copyright 2022 PrimeSieveWorkerTests.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace PrimeSieveTests;

using PrimeSieve;

[TestFixture]
public class PrimeSieveWorkerTests
{
    public static IEnumerable<object[]> NotPrimeCases
    {
        get
        {
            yield return new object[] { 4, new int[] { 3 } };
            yield return new object[] { 6, new int[] { 3, 5 } };
            yield return new object[] { 8, new int[] { 3, 5, 7 } };
            yield return new object[] { 9, new int[] { 3, 5, 7 } };
        }
    }
    
    public static IEnumerable<object[]> PrimeCases
    {
        get
        {
            yield return new object[] { 5, new int[] { 3 } };
            yield return new object[] { 7, new int[] { 3, 5 } };
            yield return new object[] { 11, new int[] { 3, 5, 7 } };
            yield return new object[] { 13, new int[] { 3, 5, 7, 11 } };
            yield return new object[] { 47, new int[] { 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43 } };
        }
    }

    private void FullRunTestHelper(int candidate, int[] primes, bool expected)
    {
        var testee = new PrimeSieveWorker();
        
        foreach (var prime in primes)
            testee.InsertNewPrime(prime);
        
        testee.PrepForRun(new TaskData(candidate));

        var result = testee.Run(new CancellationToken(false));

        Assert.That(result.Result, Is.EqualTo(expected), "Wrong primes result.");
    }

    [Test]
    public void ShouldThrowIfNotPrepped()
    {
        var testee = new PrimeSieveWorker();
        
        Assert.Throws(typeof(InvalidOperationException), () => testee.Run(new CancellationToken(false)), "Wrong exception type thrown.");
    }

    [Test]
    public void ShouldCancelBeforeStarting()
    {
        var testee = new PrimeSieveWorker();

        testee.PrepForRun((new TaskData(3)));

        var result = testee.Run(new CancellationToken(true));
        
        Assert.That(result.HasFinished, Is.False, $"{nameof(result.HasFinished)} has wrong value.");
    }

    [Test]
    [TestCaseSource(nameof(NotPrimeCases))]
    public void ShouldReturnAsNotPrime(int candidate, int[] primes)
    {
        this.FullRunTestHelper(candidate, primes, false);
    }

    [Test]
    [TestCaseSource(nameof(PrimeCases))]
    public void ShouldReturnAsPrime(int candidate, int[] primes)
    {
        this.FullRunTestHelper(candidate, primes, true);
    }
}
