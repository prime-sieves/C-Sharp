/*
   Copyright 2022 PrimeSieveTests.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace PrimeSieveTests;

using System.Text;

using PrimeSieve;

public class PrimeSieveTests
{
    public static IEnumerable<object[]> TestCases
    {
        get
        {
            yield return new object[] { 50, new Primes().To50, true };
            yield return new object[] { 10000, new Primes().To10k, true };
            
            yield return new object[] { 50, new Primes().To50, false };
            yield return new object[] { 10000, new Primes().To10k, false };
        }
    }

    private string CollectionToString(IEnumerable<int> input)
    {
        var result = new StringBuilder();
        result.Append(": [ ");
        foreach (var val in input)
            result.Append($"{val}, ");

        result.Remove(result.Length - 2, 1);
        result.Append(']');
        return result.ToString();
    }
    
    [Test]
    [TestCaseSource(nameof(TestCases))]
    public void ShouldGeneratePrimesTo(int maxValue, int[] expected, bool runInSingleThreadMode)
    {
        var testee = new PrimeSieve
        {
            MaxValue = maxValue
        };

        var actual = runInSingleThreadMode ? testee.RunInSingleThread() : testee.RunInMultiThread(12);

        Assert.That(actual.Count, Is.EqualTo(expected.Length), $"Count/Length is not equal. \nExpected: {CollectionToString(expected)}\nActual: {CollectionToString(actual)}");

        for (var i = 0; i < expected.Length; i++)
            Assert.That(actual[i], Is.EqualTo(expected[i]), $"Failed at index {i}");
    }
}
